const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const password = 'password';

const app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.send(req.params.name);
});

app.get('/:name', (req, res) => {
    res.send(req.params.name);
});

app.get(`/encode/:name`, (req, res) => {
    res.send(Vigenere.Cipher(password).crypt(req.params.name));
});

app.get(`/decode/:name`, (req, res) => {
    res.send(Vigenere.Decipher(password).crypt(req.params.name));
});

app.listen(port, () => {
    console.log('We are live on: ', 'http://127.0.0.1:8000');
});